# DrJoey

## how-people-feel-about-chch.xlsx

This web app requires a special converted version of the CCC dataset [how-people-feel-about-chch.xlsx](https://github.com/ccc-digital-channels/datasets/blob/master/GovHack2016-Christchurch/Bounty-Data/The-Identity-of-Christchurch/how-people-feel-about-chch.xlsx).

This had to be converted from XLSX to CSV for parsing inside the web app.

The converted version is inside this source control repository under the sub-folder: ~/src/DrJoey.Web/wwwroot/content/chch-feelings.csv

And can also be downloaded directly from [http://drck.azurewebsites.net/content/chch-feelings.csv](http://drck.azurewebsites.net/content/chch-feelings.csv)
