﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Data.Models
{
    internal class WatsonApiResponse
    {
        [JsonProperty(PropertyName = "document_tone")]
        public DocumentTone DocumentTone { get; set; }

        // Note: there are other elements, such as SentencesTone

    }

    internal class DocumentTone
    {
        [JsonProperty(PropertyName = "tone_categories")]
        public ToneCategory[] ToneCategories { get; set; }
    }

    internal class ToneCategory
    {
        [JsonProperty(PropertyName = "category_id")]
        public string CategoryId { get; set; }

        [JsonProperty(PropertyName = "category_name")]
        public string CategoryName { get; set; }

        [JsonProperty(PropertyName = "tones")]
        public Tone[] Tones { get; set; }
    }

    internal class Tone
    {
        [JsonProperty(PropertyName = "score")]
        public double Score { get; set; }

        [JsonProperty(PropertyName = "tone_id")]
        public string ToneId { get; set; }

        [JsonProperty(PropertyName = "tone_name")]
        public string ToneName { get; set; }
    }
}


/*
 
  "document_tone": {
    "tone_categories": [
      {
        "tones": [
          {
            "score": 0.258131,
            "tone_id": "anger",
            "tone_name": "Anger"
          }
        ],
        "category_id": "emotion_tone",
        "category_name": "Emotion Tone"
      }
    ]
  }


    */