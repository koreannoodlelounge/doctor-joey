﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Data.Models
{
    internal class WatsonApiRequest
    {
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

    }

}

