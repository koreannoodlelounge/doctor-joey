﻿using DrJoey.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DrJoey.Domain.Models;
using System.Data.SqlClient;
using System.Data;

namespace DrJoey.Data.Repositories
{
    public class SqlCaptureRepository : ICaptureRepository
    {
        public SqlCaptureRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        string _connectionString;

        public List<ToneStats> GetGroupedStats()
        {
            var tones = new List<ToneStats>();

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                string sql = "SELECT [ToneKey], COUNT([ToneKey]) FROM [dbo].[ToneIngest] GROUP BY [ToneKey]";
                using (var cmd = new SqlCommand(sql, connection))
                {
                    cmd.CommandType = CommandType.Text;
                    var rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        while (rdr.Read())
                        {
                            tones.Add(new ToneStats
                            {
                                Tone = rdr.GetString(0),
                                Count = rdr.GetInt32(1)
                            });
                        }
                    }

                }
            }

            return tones;
        }

        public void Insert(string tone)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                string sql = "INSERT INTO ToneIngest(ToneKey) VALUES(@toneKey)";
                using (var cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.Add("@toneKey", SqlDbType.VarChar).Value = tone;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void DeleteAll()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                string sql = "DELETE FROM [dbo].[ToneIngest]";
                using (var cmd = new SqlCommand(sql, connection))
                {
                    cmd.CommandType = CommandType.Text;
                    var affectedRecords = cmd.ExecuteNonQuery();
                }
            }
        }

    }
}
