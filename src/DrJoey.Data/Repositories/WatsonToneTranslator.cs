﻿using DrJoey.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using DrJoey.Data.Models;
using System.Net;
using System.Text;
using System.Net.Http.Headers;

namespace DrJoey.Data.Repositories
{
    /// <summary>
    /// API References:
    ///  - http://www.ibm.com/watson/developercloud/tone-analyzer/api/v3/#methods
    ///  - https://watson-api-explorer.mybluemix.net/apis/tone-analyzer-v3#!/tone/post_v3_tone
    /// </summary>
    public class WatsonToneTranslator : IToneTranslator
    {
        public WatsonToneTranslator(string apiUsername, string apiPassword, string apiUrl)
        {
            _apiUsername = apiUsername;
            _apiPassword = apiPassword;
            _apiUrl = apiUrl;
        }

        string _apiUsername;
        string _apiPassword;
        string _apiUrl;
        string _toneRequestUri = "/tone-analyzer/api/v3/tone?version=2016-05-19";

        const string _toneCategoryEmotional = "emotion_tone";


        public string GetTone(string message)
        {
            using (var client = buildWatsonClient())
            {
                var request = new WatsonApiRequest
                {
                    Text = message
                };

                var jsonRequestContent = JsonConvert.SerializeObject(request);
                var postContent = new StringContent(jsonRequestContent, Encoding.UTF8, "application/json");

                var response = client.PostAsync(_toneRequestUri, postContent).GetAwaiter().GetResult();

                response.EnsureSuccessStatusCode(); // Throw in not success

                var stringResponse = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var responseData = JsonConvert.DeserializeObject<WatsonApiResponse>(stringResponse);

                if (responseData.DocumentTone == null
                    || responseData.DocumentTone.ToneCategories == null
                    || responseData.DocumentTone.ToneCategories.Length == 0)
                    throw new Exception("Couldn't parse watson data correctly (1)");

                var emoToneCategory = responseData.DocumentTone.ToneCategories.FirstOrDefault(c => c.CategoryId == _toneCategoryEmotional);
                if (emoToneCategory == null)
                    throw new Exception("Couldn't parse watson data correctly (2)");

                var maxScoreTone = emoToneCategory.Tones.OrderByDescending(t => t.Score).FirstOrDefault();
                if (maxScoreTone == null || string.IsNullOrWhiteSpace(maxScoreTone.ToneName))
                    throw new Exception("Couldn't parse watson data correctly (3)");

                return maxScoreTone.ToneName;
            }
        }

        HttpClient buildWatsonClient()
        {
            var auth = Convert.ToBase64String(Encoding.ASCII.GetBytes(_apiUsername + ":" + _apiPassword));

            var client = new HttpClient(new HttpClientHandler()
            {
                AllowAutoRedirect = false,
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                UseCookies = true,
                CookieContainer = new CookieContainer()
            });

            client.BaseAddress = new Uri(_apiUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", auth);

            return client;
        }
    }

}
