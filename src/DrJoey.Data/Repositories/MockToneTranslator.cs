﻿using DrJoey.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Data.Repositories
{
    public class MockToneTranslator : IToneTranslator
    {
        public MockToneTranslator(string toneToReturn)
        {
            _toneToReturn = toneToReturn;
        }

        string _toneToReturn;

        public string GetTone(string message)
        {
            return _toneToReturn;
        }

    }
}
