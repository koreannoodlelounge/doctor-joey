﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DrJoey.Web.Infrastructure;
using DrJoey.Domain.Services;
using DrJoey.Domain.Models;
using DrJoey.Domain.Repositories;
using DrJoey.Data.Repositories;

namespace DrJoey.Web
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            // App Settings configuration
            services.AddOptions();
            services.Configure<ServiceSettings>(Configuration.GetSection("ServiceSettings"));

            // Watson tone analyzer
            services.AddSingleton<LocationSettings>(new LocationSettings
            {
                Latitude = Configuration.GetValue<double>("ServiceSettings:socialLatitude"),
                Longitude = Configuration.GetValue<double>("ServiceSettings:socialLongitude"),
                SurveyRadius = Configuration.GetValue<double>("ServiceSettings:socialRadius")
            });
            var watsonUsername = Configuration.GetValue<string>("ServiceSettings:watsonUsername");
            var watsonPassword = Configuration.GetValue<string>("ServiceSettings:watsonPassword");
            var watsonApiUrl = Configuration.GetValue<string>("ServiceSettings:watsonApiUrl");
            var useWatson = Configuration.GetValue<bool>("ServiceSettings:useWatson");
            if (useWatson)
            {
                services.AddSingleton<IToneTranslator>(new WatsonToneTranslator(watsonUsername, watsonPassword, watsonApiUrl));
            }
            else
            {
                var fudgeTone = Configuration.GetValue<string>("ServiceSettings:watsonFudgeTone");
                services.AddSingleton<IToneTranslator>(new MockToneTranslator(toneToReturn: fudgeTone));
            }


            // Capture repo
            var connString = Configuration.GetConnectionString("MainSqlServer");
            services.AddSingleton<ICaptureRepository>(new SqlCaptureRepository(connString));

            // Services
            services.AddSingleton<ToneGraphService>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
