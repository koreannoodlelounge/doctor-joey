﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DrJoey.Domain.Services;
using Microsoft.AspNetCore.Http;
using System.IO;
using DrJoey.Domain.Repositories;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DrJoey.Web.Controllers
{
    public class GraphController : Controller
    {
        public GraphController(ToneGraphService toneGraphService, ICaptureRepository captureRepository)
        {
            _toneGraphService = toneGraphService;
            _captureRepository = captureRepository;
        }

        ToneGraphService _toneGraphService;
        ICaptureRepository _captureRepository;


        // GET: graph/stats
        [HttpGet]
        [ActionName("Stats")]
        public IActionResult GetStats()
        {
            var graph = _toneGraphService.GetGraph();
            return Json(graph);
        }
        
        // GET: graph/cleardata
        [HttpGet]
        [ActionName("ClearData")]
        public IActionResult ClearData()
        {
            // Delete all tone graph data
            _captureRepository.DeleteAll();

            return Ok();
        }

    }
}
