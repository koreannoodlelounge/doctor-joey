﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.IO;
using DrJoey.Domain.Services;
using DrJoey.Domain.Repositories;
using DrJoey.Web.Infrastructure;
using Microsoft.Extensions.Options;
using System.Diagnostics;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace DrJoey.Web.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(ToneGraphService toneGraphService, ICaptureRepository captureRepository, IOptions<ServiceSettings> settings)
        {
            _toneGraphService = toneGraphService;
            _captureRepository = captureRepository;
            _settings = settings.Value;
        }

        ServiceSettings _settings;
        ToneGraphService _toneGraphService;
        ICaptureRepository _captureRepository;


        // GET: /
        public IActionResult Index()
        {
            return View();
        }

        // POST: processfile
        [HttpPost]
        [ActionName("ProcessFile")]
        public IActionResult ProcessFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                throw new Exception("No file specified");

            var csvData = string.Empty;

            using (var strm = file.OpenReadStream())
            {
                using (var rdr = new StreamReader(strm))
                {
                    csvData = rdr.ReadToEnd();
                }
            }

            if (string.IsNullOrWhiteSpace(csvData))
                throw new Exception("Could not parse uploaded file");

            if (_settings.CsvLinesToProcess > 0)
            {
                _toneGraphService.ProcessSurveyCsv(csvData, take: _settings.CsvLinesToProcess);
            }
            else
            {
                _toneGraphService.ProcessSurveyCsv(csvData);
            }

            return RedirectToAction("Index");
        }

        // POST: processcsv
        [HttpPost]
        [ActionName("ProcessString")]
        public IActionResult ProcessString(string message, string fudgedTone)
        {
            _toneGraphService.ProcessTone(message, fudgedTone);

            return RedirectToAction("Index");
        }
    }
}
