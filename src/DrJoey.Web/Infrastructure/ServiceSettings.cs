﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Web.Infrastructure
{
    public class ServiceSettings
    {
        public int CsvLinesToProcess { get; set; }
        public string WatsonApiUrl { get; set; }
        public string WatsonPassword { get; set; }
        public string WatsonUsername { get; set; }
        public double SocialLongitude { get; set; }
        public double SocialLatitude { get; set; }
        public double SocialRadius { get; set; }
    }
}
