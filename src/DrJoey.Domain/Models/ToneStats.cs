﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Domain.Models
{
    public class ToneStats
    {
        public string Tone { get; set; }
        public int Count { get; set; }
    }
}
