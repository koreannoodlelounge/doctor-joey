﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Domain.Models
{
    public class LocationSettings
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double SurveyRadius { get; set; }
    }
}
