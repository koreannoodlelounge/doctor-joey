﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Domain.Repositories
{
    public interface IToneTranslator
    {
        string GetTone(string message);
    }
}
