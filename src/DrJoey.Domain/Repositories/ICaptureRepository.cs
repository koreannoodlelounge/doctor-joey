﻿using DrJoey.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Domain.Repositories
{
    public interface ICaptureRepository
    {
        List<ToneStats> GetGroupedStats();
        void Insert(string tone);
        void DeleteAll();
    }
}
