﻿using DrJoey.Domain.Models;
using DrJoey.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DrJoey.Domain.Services
{
    public class ToneGraphService
    {
        public ToneGraphService(LocationSettings settings, IToneTranslator translator, ICaptureRepository captureRepository)
        {
            _settings = settings;
            _translator = translator;
            _captureRepository = captureRepository;
        }

        LocationSettings _settings;
        IToneTranslator _translator;
        ICaptureRepository _captureRepository;


        public List<Models.ToneStats> GetGraph()
        {
            return _captureRepository.GetGroupedStats();
        }

        public void ProcessTone(string message, string fudgedTone = "")
        {
            var tone = string.Empty;

            if (string.IsNullOrWhiteSpace(fudgedTone))
                tone = _translator.GetTone(message);
            else
                tone = fudgedTone;

            Debug.WriteLine("[" + (tone ?? "") + "] " + message);
                
            _captureRepository.Insert(tone);
        }

        public void ProcessSurveyCsv(string csvData, int take = -1)
        {
            // TODO: Parse CSV file

            var csvArr = csvData.Split("\r\n".ToArray(), StringSplitOptions.RemoveEmptyEntries);

            // Take x rows if specified
            // First 2 rows are headers
            var lines = take == -1 ? csvArr.Skip(2) : csvArr.Skip(2).Take(take);

            // First two lines are headers
            foreach (var line in lines)
            {
                var cells = line.Split(';');
                if (cells.Length < 31)
                    throw new Exception();

                var longMsg = (cells[29] ?? "").Trim().Trim('"');

                if (string.IsNullOrWhiteSpace(longMsg))
                    continue;

                var message1 = longMsg.Trim().Trim('"').TrimEnd('.');
                
                var message2 = (cells[30] ?? "").Trim().Trim('"').TrimEnd('.');
                if (!string.IsNullOrWhiteSpace(message2))
                    message2 += ".";

                var msg = string.Format("{0}. {1}", message1, message2).TrimEnd();

                ProcessTone(msg);
            }

        }

    }
}
